FROM nginx:1.10

ADD css/ /srv/landing-page/css
ADD font-awesome/ /srv/landing-page/font-awesome
ADD fonts/ /srv/landing-page/fonts
ADD img/ /srv/landing-page/img
ADD js/ /srv/landing-page/js
ADD index.html /srv/landing-page/index.html

ADD nginx.conf /etc/nginx/conf.d/default.conf 

EXPOSE 80
